package com.atlassian.rx.queue.sqs.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class SqsMessagePublisherTest {

    private static final SendMessageRequest REQUEST = new SendMessageRequest()
            .withQueueUrl("queue")
            .withMessageBody("body")
            .withDelaySeconds(10);
    private static final SendMessageResult RESULT = new SendMessageResult();

    @Rule
    public MockitoRule initMocks = MockitoJUnit.rule();

    @Mock
    private AmazonSQS sqsClient;

    private SqsEventPublisher publisher;

    @Before
    public void setUp() throws Exception {

        when(sqsClient.sendMessage(eq(REQUEST))).thenReturn(RESULT);

        publisher = new SqsEventPublisher(sqsClient);
    }

    @Test
    public void publish() {
        publisher.publish(REQUEST)
                .test()
                .assertValue(RESULT);
    }
}