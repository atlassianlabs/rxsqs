package com.atlassian.rx.queue;

import io.reactivex.annotations.NonNull;

import java.util.function.Supplier;

/**
 * Represents a supplier of events from the underlying system
 */
@FunctionalInterface
public interface EventSupplier<EventType> extends Supplier<EventType> {

    /**
     * Gets an event, blocking until it's available
     *
     * @return an event, never null
     */
    @Override
    @NonNull
    EventType get();
}
