package com.atlassian.rx.queue.sqs.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SqsMessageSupplierTest {

    private static final String BODY = "body";
    private static final Message SQS_MESSAGE = new Message().withBody(BODY);
    private static final String QUEUE_URL = "queueUrl";

    @Rule
    public MockitoRule initMocks = MockitoJUnit.rule();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private AmazonSQS sqs;

    private SqsEventSupplier supplier;

    @Before
    public void setUp() {
        final ReceiveMessageResult receiveMessageResult = new ReceiveMessageResult().withMessages(SQS_MESSAGE);

        when(sqs.receiveMessage(any(ReceiveMessageRequest.class))).thenReturn(receiveMessageResult);
        when(sqs.deleteMessage(any(DeleteMessageRequest.class))).thenReturn(new DeleteMessageResult());

        supplier = new SqsEventSupplier(sqs, QUEUE_URL, 0);
    }

    @Test
    public void get_fetchesFromQueue() {
        supplier.get();

        verify(sqs).receiveMessage(any(ReceiveMessageRequest.class));
    }

    @Test
    public void get_refetchesFromQueue_whenRequested() {
        supplier.get();
        supplier.get();
        supplier.get();

        verify(sqs, times(3)).receiveMessage(any(ReceiveMessageRequest.class));
    }

    @Test
    public void get_handlesErrorFromSqs() {
        final ReceiveMessageResult receiveMessageResult = new ReceiveMessageResult()
                .withMessages(SQS_MESSAGE);
        final ReceiveMessageResult emptyReceiveMessageResult = new ReceiveMessageResult();

        when(sqs.receiveMessage(any(ReceiveMessageRequest.class))).thenThrow(new RuntimeException("Bad SQS!"))
                .thenReturn(emptyReceiveMessageResult)
                .thenReturn(receiveMessageResult);

        final Message message = supplier.get();

        assertThat(message, is(not(nullValue())));
        verify(sqs, times(3)).receiveMessage(any(ReceiveMessageRequest.class));
    }

    @Test
    public void get_throwsInterruptedException() {
        thrown.expect(RuntimeException.class);

        final ReceiveMessageResult receiveMessageResult = new ReceiveMessageResult()
                .withMessages(SQS_MESSAGE);
        final ReceiveMessageResult emptyReceiveMessageResult = new ReceiveMessageResult();

        when(sqs.receiveMessage(any(ReceiveMessageRequest.class))).thenThrow(new RuntimeException("Bad SQS!", new InterruptedException("Bad SDK!")))
                .thenReturn(emptyReceiveMessageResult)
                .thenReturn(receiveMessageResult);

        supplier.get();
    }
}