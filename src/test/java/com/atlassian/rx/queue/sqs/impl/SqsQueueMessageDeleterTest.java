package com.atlassian.rx.queue.sqs.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import com.amazonaws.services.sqs.model.Message;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class SqsQueueMessageDeleterTest {

    private static final DeleteMessageResult DELETE_RESULT = new DeleteMessageResult();
    private static final String HANDLE = "handle";
    private static final String QUEUE = "queue";

    @Rule
    public MockitoRule initMocks = MockitoJUnit.rule();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private AmazonSQS sqs;

    private SqsQueueEventDeleter deleter;

    @Before
    public void setUp() {
        final DeleteMessageRequest request = new DeleteMessageRequest()
                .withReceiptHandle(HANDLE)
                .withQueueUrl(QUEUE);

        when(sqs.deleteMessage(eq(request))).thenReturn(DELETE_RESULT);

        deleter = new SqsQueueEventDeleter(sqs, QUEUE);
    }

    @Test
    public void deleteInvokesSqs() {
        final Message message = new Message()
                .withReceiptHandle(HANDLE);

        deleter.delete(message)
                .test()
                .assertComplete()
                .assertValue(DELETE_RESULT);
    }


    @Test
    public void deleteEncapsulatesError() {
        final RuntimeException exception = new RuntimeException("No!");
        when(sqs.deleteMessage(any())).thenThrow(exception);

        final Message message = new Message();
        deleter.delete(message)
                .test()
                .assertError(exception);
    }
}