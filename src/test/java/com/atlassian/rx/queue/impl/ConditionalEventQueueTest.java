package com.atlassian.rx.queue.impl;

import com.atlassian.rx.queue.EventSupplier;
import com.atlassian.rx.queue.impl.ConditionalEventQueue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ConditionalEventQueueTest {

    private static final String EVENT1 = "event1";
    private static final String EVENT2 = "event2";

    @Rule
    public MockitoRule initMocks = MockitoJUnit.rule();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private EventSupplier<String> supplier;

    @Mock
    private Runnable runnable;

    private ConditionalEventQueue<String> eventQueue;

    @Before
    public void setUp() throws Exception {
        when(supplier.get()).thenReturn(EVENT1);

        eventQueue = new ConditionalEventQueue<>(supplier, runnable);
    }

    @Test
    public void getAllEvents_fetchesFromQueue() throws Exception {
        eventQueue.getAllEvents()
                .test(1)
                .assertValueCount(1)
                .assertValue(EVENT1);

        verify(runnable).run();
    }

    @Test
    public void getAllEvents_refetchesFromSupplier_whenRequested() throws Exception {
        when(supplier.get()).thenReturn(EVENT1).thenReturn(EVENT2);

        final int times = 2;
        eventQueue.getAllEvents()
                .test(times)
                .assertValues(EVENT1, EVENT2);

        verify(runnable, times(times)).run();
    }

    @Test
    public void getAllEvents_handlesErrorFromSupplier() throws Exception {
        final RuntimeException error = new RuntimeException("Boh!");
        when(supplier.get()).thenThrow(error);

        eventQueue.getAllEvents()
                .test(1)
                .assertError(error);
    }
}