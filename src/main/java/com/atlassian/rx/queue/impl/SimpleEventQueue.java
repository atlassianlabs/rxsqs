package com.atlassian.rx.queue.impl;

import com.atlassian.rx.queue.EventQueue;
import com.atlassian.rx.queue.EventSupplier;
import io.reactivex.Flowable;

import static java.util.Objects.requireNonNull;

/**
 * An event queue that always fetches from the supplier
 */
public class SimpleEventQueue<T> implements EventQueue<T> {

    private final EventSupplier<T> eventSupplier;

    /**
     * Creates an event queue
     *
     * @param eventSupplier the supplier of events
     */
    public SimpleEventQueue(final EventSupplier<T> eventSupplier) {
        this.eventSupplier = requireNonNull(eventSupplier);
    }

    @Override
    public Flowable<T> getAllEvents() {
        return Flowable.generate(messageEmitter -> messageEmitter.onNext(eventSupplier.get()));
    }
}
