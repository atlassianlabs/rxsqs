RxSQS
===========================================
The **RxSQS** library lets you wrap SQS inside of an RxJava `Flowable`, which helps you keep event processing clean and resilient.

NOTE: This is not yet open source, but will be made public soon, the license etc is just in preparation 

Use this library to:

* Build your event processing on top of an infinite stream with configurable concurrency
* Fetch messages from SQS using [reactive pull](https://github.com/ReactiveX/RxJava/wiki/Backpressure)
* Publish messages to SQS using a [cold](https://github.com/Froussios/Intro-To-RxJava/blob/master/Part%203%20-%20Taming%20the%20sequence/6.%20Hot%20and%20Cold%20observables.md) `Flowable`
* Delete messages from SQS using a cold `Flowable` 

For an example implementation see [here](https://bitbucket.org/ccrolf/rxsqs/src/master/src/main/java/com/atlassian/rx/queue/Example.java). Note the error handling, as the weak point of this approach is that if the downstream processing fails, the consumption
of messages from SQS may stop.

## Getting Started

Add the following dependency and replace your current approach to getting messages off of SQS. Your new approach should look similar to the migration in this [example](https://bitbucket.org/ccrolf/rxsqs/src/master/src/main/java/com/atlassian/rx/queue/Example.java).
```xml
  <dependency>
    <groupId>com.atlassian</groupId>
    <artifactId>rx-sqs</artifactId>
    <version>${rx-sqs.version}</version>
  </dependency>
```

## Feedback
* Give us feedback [here](https://bitbucket.org/ccrolf/rxsqs).


## Future
We will add more functionality over time. Including a restarting subscriber for the unknown unknowns that may break your event processing

Other features will be considered if they're added to the issues. 


## Building and testing ##

The project uses Maven 3.2.5+. We recommend using [mvnvm](http://mvnvm.org/) or similar.

To build the project:

```
>> mvn clean compile
```

To run the project tests:

```
>> mvn test
```

## Contributors ##

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes, please ensure that the test coverage hasn't dropped
* Follow the existing style (checkstyle checking is enabled by default in builds)
* Separate unrelated changes into multiple pull requests

Please ensure that your branch builds successfully before you open your PR. The Pipelines build won't run by default 
on a remote branch, so either enable Pipelines for your fork or run the build locally: 

```
mvn clean verify javadoc:javadoc
```

See the existing [issues](https://bitbucket.org/ccrolf/rxsqs/issues) to start
contributing. If you want to start working on an issue, please assign the ticket to yourself and mark it as `open`
so others know it is in progress.

For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

## License ##

Copyright (c) 2018 Atlassian and others. Apache 2.0 licensed, see LICENSE.txt file.
