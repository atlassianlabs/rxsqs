package com.atlassian.rx.queue.impl;

import com.atlassian.rx.queue.EventQueue;
import com.atlassian.rx.queue.EventSupplier;
import io.reactivex.Flowable;

import static java.util.Objects.requireNonNull;

/**
 * An event queue that waits before a condition every time before fetching an event
 */
public class ConditionalEventQueue<T> implements EventQueue<T> {

    private final EventSupplier<T> eventSupplier;
    private final Runnable waitCondition;

    /**
     * Creates an event queue
     *
     * @param eventSupplier the supplier of events
     * @param waitCondition the condition to wait for before fetching an event, e.g., a feature flag becoming true
     */
    public ConditionalEventQueue(final EventSupplier<T> eventSupplier,
                                 final Runnable waitCondition) {
        this.eventSupplier = requireNonNull(eventSupplier);
        this.waitCondition = requireNonNull(waitCondition);
    }

    @Override
    public Flowable<T> getAllEvents() {
        return Flowable.generate(messageEmitter -> messageEmitter.onNext(getMessageAfterWaitingForCondition()));
    }

    private T getMessageAfterWaitingForCondition() {
        waitCondition.run();

        return eventSupplier.get();
    }
}
