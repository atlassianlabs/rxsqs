package com.atlassian.rx.queue;

import io.reactivex.Flowable;

/**
 * Represents a potentially infinite queue of events
 *
 * @param <EventType> the type of events
 */
@FunctionalInterface
public interface EventQueue<EventType> {

    /**
     * Retrieves all event from the queue, using reactive pull. Hence, more events are only fetched when downstream consumers request them
     *
     * @return a {@link Flowable} of events
     */
    Flowable<EventType> getAllEvents();
}
