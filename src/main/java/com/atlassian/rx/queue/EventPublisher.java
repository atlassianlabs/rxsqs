package com.atlassian.rx.queue;

import io.reactivex.Flowable;

/**
 * Represents a publisher of events to the underlying system
 *
 * @param <EventType>     the event type
 * @param <PublishResult> the result of publishing the event
 */
@FunctionalInterface
public interface EventPublisher<EventType, PublishResult> {

    /**
     * Publishes an event to the underlying system
     *
     * @param event the event to publish
     * @return a cold {@link Flowable} containing the result of publishing the event, or {@link Flowable#error(Throwable)} if an exception occurred
     */
    Flowable<PublishResult> publish(EventType event);
}
