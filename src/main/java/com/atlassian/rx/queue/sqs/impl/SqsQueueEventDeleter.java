package com.atlassian.rx.queue.sqs.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import com.amazonaws.services.sqs.model.Message;
import io.reactivex.Flowable;

import static java.util.Objects.requireNonNull;

public class SqsQueueEventDeleter implements com.atlassian.rx.queue.sqs.SqsEventDeleter<Message> {

    private final AmazonSQS sqsClient;
    private final String queueUrl;

    public SqsQueueEventDeleter(final AmazonSQS sqsClient, final String queueUrl) {
        this.sqsClient = requireNonNull(sqsClient);
        this.queueUrl = requireNonNull(queueUrl);
    }

    @Override
    public Flowable<DeleteMessageResult> delete(final Message message) {
        return Flowable.fromCallable(() -> {
            final DeleteMessageRequest request = createDeleteRequest(message);
            return sqsClient.deleteMessage(request);
        });
    }

    private DeleteMessageRequest createDeleteRequest(final Message message) {
        return new DeleteMessageRequest()
                .withQueueUrl(queueUrl)
                .withReceiptHandle(message.getReceiptHandle());
    }
}
