package com.atlassian.rx.queue.sqs;

import com.amazonaws.services.sqs.model.DeleteMessageResult;
import com.atlassian.rx.queue.EventDeleter;

/**
 * Represents a deleter of events from SQS
 *
 * @param <Request> the deletion request type
 */
@FunctionalInterface
public interface SqsEventDeleter<Request> extends EventDeleter<Request, DeleteMessageResult> {
}
