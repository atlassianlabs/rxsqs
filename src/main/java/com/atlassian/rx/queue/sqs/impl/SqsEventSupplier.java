package com.atlassian.rx.queue.sqs.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.util.Throwables;
import io.reactivex.annotations.NonNull;
import org.slf4j.Logger;

import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Wraps SQS in a Supplier of Message. Messages are read one at a time, waiting the provided wait time.
 */
public class SqsEventSupplier implements com.atlassian.rx.queue.sqs.SqsEventSupplier {

    private static final Logger log = getLogger(SqsEventSupplier.class);

    private final AmazonSQS sqsClient;
    private final String queueUrl;
    private final int waitTimeSeconds;

    /**
     * Creates an event supplier fetching messages from the specified queue
     *
     * @param sqsClient       the client for interacting with SQS
     * @param queueUrl        the URL of the queue to fetch messages from
     * @param waitTimeSeconds the time to wait for messages before returning
     */
    public SqsEventSupplier(final AmazonSQS sqsClient,
                            final String queueUrl,
                            final int waitTimeSeconds) {
        this.sqsClient = requireNonNull(sqsClient);
        this.queueUrl = requireNonNull(queueUrl);
        this.waitTimeSeconds = waitTimeSeconds;
    }

    @Override
    @NonNull
    public Message get() {
        final ReceiveMessageRequest request = createReceiveRequest();
        return receiveMessageBlocking(request);
    }

    private ReceiveMessageRequest createReceiveRequest() {
        return new ReceiveMessageRequest()
                .withQueueUrl(queueUrl)
                .withWaitTimeSeconds(waitTimeSeconds)
                .withAttributeNames("All")
                .withMessageAttributeNames("All");
    }

    private Message receiveMessageBlocking(final ReceiveMessageRequest request) {
        List<Message> messages;

        do {
            messages = receiveMessageSafely(request); //We don't need to wait/sleep here, cause waitTimeSeconds > 0 will ensure we don't busy/wait
        } while (messages.isEmpty());

        return messages.get(0);
    }

    private List<Message> receiveMessageSafely(final ReceiveMessageRequest request) {
        try {
            return sqsClient.receiveMessage(request).getMessages();
        } catch (final RuntimeException e) {
            log.warn("Caught exception when trying to read from SQS queue {}:", queueUrl, e);
            handleInterruptedException(e);
            return Collections.emptyList();
        }
    }

    //The AWS SDK wraps interrupted exceptions, we need to throw when we get one to avoid polling with interrupted threads
    private void handleInterruptedException(final RuntimeException e) {
        final Throwable rootCause = Throwables.getRootCause(e);
        if (rootCause instanceof InterruptedException || Thread.currentThread().isInterrupted()) {
            throw e;
        }
    }
}
