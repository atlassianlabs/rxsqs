package com.atlassian.rx.queue;

import io.reactivex.Flowable;

/**
 * Represents a deleter of events from the underlying event system
 *
 * @param <Request> the deletion request type
 * @param <Result>  the deletion result
 */
@FunctionalInterface
public interface EventDeleter<Request, Result> {

    /**
     * Deletes a events based on the provided request
     *
     * @param request the deletion request
     * @return a cold {@link Flowable} containing the result of the deletetion, or {@link Flowable#error(Throwable)} if an exception occurred
     */
    Flowable<Result> delete(Request request);
}
