package com.atlassian.rx.queue.sqs;

import com.amazonaws.services.sqs.model.Message;
import com.atlassian.rx.queue.EventSupplier;

/**
 * Represents a supplier of events from SQS
 */
@FunctionalInterface
public interface SqsEventSupplier extends EventSupplier<Message> {

}
