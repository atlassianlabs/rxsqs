package com.atlassian.rx.queue.sqs.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import io.reactivex.Flowable;

import static java.util.Objects.requireNonNull;

public class SqsQueueEventPublisher implements com.atlassian.rx.queue.sqs.SqsEventPublisher<Message> {

    private final AmazonSQS sqsClient;
    private final String queueUrl;
    private final int visibilityDelaySeconds;

    public SqsQueueEventPublisher(final AmazonSQS sqsClient, final String queueUrl) {
        this(sqsClient, queueUrl, 0);
    }

    public SqsQueueEventPublisher(final AmazonSQS sqsClient, final String queueUrl,
                                  final int visibilityDelaySeconds) {
        this.sqsClient = requireNonNull(sqsClient);
        this.queueUrl = requireNonNull(queueUrl);
        this.visibilityDelaySeconds = visibilityDelaySeconds;
    }

    @Override
    public Flowable<SendMessageResult> publish(final Message message) {
        return Flowable.fromCallable(() -> {
            final SendMessageRequest request = createPublishRequest(message);

            return sqsClient.sendMessage(request);
        });
    }

    private SendMessageRequest createPublishRequest(final Message message) {
        return new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withDelaySeconds(visibilityDelaySeconds)
                .withMessageBody(message.getBody())
                .withMessageAttributes(message.getMessageAttributes());
    }
}
