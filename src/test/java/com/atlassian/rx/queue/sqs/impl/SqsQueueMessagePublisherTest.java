package com.atlassian.rx.queue.sqs.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.Map;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class SqsQueueMessagePublisherTest {

    private static final SendMessageResult RESULT = new SendMessageResult();
    private static final String BODY = "body";
    private static final Integer DELAY = 10;
    private static final Map<String, MessageAttributeValue> ATTRIBUTES = Collections.singletonMap("key", new MessageAttributeValue()
            .withStringValue("value")
            .withDataType("String"));
    private static final String QUEUE = "queue";

    @Rule
    public MockitoRule initMocks = MockitoJUnit.rule();

    @Mock
    private AmazonSQS sqsClient;

    private SqsQueueEventPublisher publisher;

    @Before
    public void setUp() throws Exception {
        final SendMessageRequest sendMessageRequest = new SendMessageRequest()
                .withMessageBody(BODY)
                .withDelaySeconds(DELAY)
                .withMessageAttributes(ATTRIBUTES)
                .withQueueUrl(QUEUE);

        when(sqsClient.sendMessage(eq(sendMessageRequest))).thenReturn(RESULT);

        publisher = new SqsQueueEventPublisher(sqsClient, QUEUE, DELAY);
    }

    @Test
    public void publish() {
        final Message message = new Message()
                .withBody(BODY)
                .withMessageAttributes(ATTRIBUTES);

        publisher.publish(message)
                .test()
                .assertValue(RESULT);
    }
}