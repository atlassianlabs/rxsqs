package com.atlassian.rx.queue.sqs;

import com.amazonaws.services.sqs.model.Message;
import com.atlassian.rx.queue.EventQueue;

/**
 * Represents a potentially infinite queue of messages from SQS
 */
@FunctionalInterface
public interface SqsEventQueue extends EventQueue<Message> {

}
