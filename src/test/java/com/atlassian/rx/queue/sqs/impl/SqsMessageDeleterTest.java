package com.atlassian.rx.queue.sqs.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class SqsMessageDeleterTest {

    private static final DeleteMessageResult DELETE_RESULT = new DeleteMessageResult();

    @Rule
    public MockitoRule initMocks = MockitoJUnit.rule();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private AmazonSQS sqs;

    private SqsEventDeleter deleter;

    @Before
    public void setUp() {
        when(sqs.deleteMessage(any())).thenReturn(DELETE_RESULT);

        deleter = new SqsEventDeleter(sqs);
    }

    @Test
    public void deleteInvokesSqs() {
        final DeleteMessageRequest request = new DeleteMessageRequest();
        deleter.delete(request)
                .test()
                .assertComplete()
                .assertValue(DELETE_RESULT);
    }


    @Test
    public void deleteEncapsulatesError() {
        final RuntimeException exception = new RuntimeException("No!");
        when(sqs.deleteMessage(any())).thenThrow(exception);

        final DeleteMessageRequest request = new DeleteMessageRequest();
        deleter.delete(request)
                .test()
                .assertError(exception);
    }
}