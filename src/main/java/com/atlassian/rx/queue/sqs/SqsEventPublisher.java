package com.atlassian.rx.queue.sqs;

import com.amazonaws.services.sqs.model.SendMessageResult;
import com.atlassian.rx.queue.EventPublisher;

/**
 * Represents a publisher of events to SQS
 *
 * @param <EventType> the event type
 */
@FunctionalInterface
public interface SqsEventPublisher<EventType> extends EventPublisher<EventType, SendMessageResult> {

}
