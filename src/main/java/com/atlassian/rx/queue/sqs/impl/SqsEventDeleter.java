package com.atlassian.rx.queue.sqs.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import io.reactivex.Flowable;

import static java.util.Objects.requireNonNull;

public class SqsEventDeleter implements com.atlassian.rx.queue.sqs.SqsEventDeleter<DeleteMessageRequest> {

    private final AmazonSQS sqsClient;

    public SqsEventDeleter(final AmazonSQS sqsClient) {
        this.sqsClient = requireNonNull(sqsClient);
    }

    @Override
    public Flowable<DeleteMessageResult> delete(final DeleteMessageRequest request) {
        return Flowable.fromCallable(() -> sqsClient.deleteMessage(request));
    }
}
