package com.atlassian.rx.queue.sqs.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import io.reactivex.Flowable;

public class SqsEventPublisher implements com.atlassian.rx.queue.sqs.SqsEventPublisher<SendMessageRequest> {

    private final AmazonSQS sqsClient;

    public SqsEventPublisher(final AmazonSQS sqsClient) {
        this.sqsClient = sqsClient;
    }

    @Override
    public Flowable<SendMessageResult> publish(final SendMessageRequest event) {
        return Flowable.fromCallable(() -> sqsClient.sendMessage(event));
    }
}
