package com.atlassian.rx.queue;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.atlassian.rx.queue.impl.SimpleEventQueue;
import com.atlassian.rx.queue.sqs.SqsEventDeleter;
import com.atlassian.rx.queue.sqs.impl.SqsEventSupplier;
import com.atlassian.rx.queue.sqs.impl.SqsQueueEventDeleter;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Example of migration from non-reactive SQS event processing, to the reactive way.
 */
public class Example {

    /**
     * This is the typical way that SQS messages are processed in a non-reactive fashion. No parallelization of the message processing
     */
    public void processMessages(AmazonSQS sqs, String queueUrl, EventProcessor eventProcessor) {
        while (true) {
            ReceiveMessageResult messages = sqs.receiveMessage(queueUrl);
            for (Message message : messages.getMessages()) {
                try {
                    ProcessingResult result = eventProcessor.process(message);
                    DeleteMessageResult deleteMessageResult = sqs.deleteMessage(queueUrl, message.getReceiptHandle());
                } catch (RuntimeException ignored) {
                    //Handle the case of the processing failing
                }
            }
        }
    }

    /**
     * The reactive way to process messages using reactive pull. Will be parallelized to the size of the executor's thread pool
     */
    public void processMessagesReactive(AmazonSQS sqs, String queueUrl, EventProcessor eventProcessor) {
        EventSupplier<Message> eventSupplier = new SqsEventSupplier(sqs, queueUrl, 0);
        EventQueue<Message> queue = new SimpleEventQueue<>(eventSupplier);
        SqsEventDeleter<Message> deleter = new SqsQueueEventDeleter(sqs, queueUrl);
        Executor executor = Executors.newFixedThreadPool(8);            //The maximum number of messages that will be in flight at any given time

        queue.getAllEvents()
                .flatMap(message -> Flowable.just(message)              //The message needs to be wrapped, so we can delete after processing
                        .map(eventProcessor::process)                   //Process the message
                        .flatMap(result -> deleter.delete(message)))    //Delete the message, if the processing was successful
                .onErrorResumeNext(Flowable.empty())                    //Handle potential errors thrown by the eventProcessor (prevents the stream from stopping)
                .subscribeOn(Schedulers.from(executor))                 //Parallelizes the processing
                .subscribe();                                           //Starts the processing of messages
    }

    /**
     * Represents a processing service
     */
    public static class EventProcessor {
        public ProcessingResult process(Message message) {
            return new ProcessingResult(message);
        }
    }

    /**
     * Value object for the processing result
     */
    public static class ProcessingResult {
        private final Message message;

        public ProcessingResult(Message message) {
            this.message = message;
        }

        public Message getMessage() {
            return message;
        }
    }
}
